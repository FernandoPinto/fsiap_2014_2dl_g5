package logic;

import java.io.Serializable;

public class Calculus implements Serializable {
    private double i, iz, malu_ang;
    private double n1, n2, brew_ang;
    
    public Calculus(double i, double iz, double malu_ang, double n1, double n2, double brew_ang){
        this.i = i;
        this.iz = iz;
        this.malu_ang = malu_ang;
        this.n1 = n1;
        this.n2 = n2;
        this.brew_ang = brew_ang;
    }
    
    public Calculus(Calculus c){
        this.i = c.getI();
        this.iz = c.getIz();
        this.malu_ang = c.getMalu_ang();
    }
    
    public Calculus(){
        this.i = 0;
        this.iz = 0;
        this.malu_ang = 0;
        this.n1 = 0;
        this.n2 = 0;
        this.brew_ang = 0;
    }
    
    public boolean equals(Calculus c){
        return (this.getI()==c.getI() && this.getIz()==c.getIz() && this.getMalu_ang()==c.getMalu_ang());
    }
    
    // ANG = arcos(sqrt(I / I0))
    public static double Intensity(double i, double iz, Object obj){
        return Math.acos(Math.sqrt(i/iz))*180/Math.PI;
    }   
    
    // I0 = I / cos^2( ANG )
    public static double Intensity(double i, Object obj, double ang){
        return i / Math.pow((Math.cos(Math.toRadians(ang))), 2);
    }
    
    // I = I0 * cos^2( ANG )
    public static double Intensity(Object obj, double iz, double ang){
        return iz * Math.pow((Math.cos(Math.toRadians(ang))), 2);
    }
    
    // B = arctg (n2 / n1)
    public static double Brewster(Object obj, double n1, double n2){
        return Math.toDegrees(Math.atan(n2 / n1));
    }
    
    // n1 = n2 / tan(B)
    public static double Brewster(double ang, Object obj, double n2){
        return n2 / Math.tan(Math.toRadians(ang));
    }
    
     // n2 = tan(B) * n1
    public static double Brewster(double ang, double n1, Object obj){
        return Math.tan(Math.toRadians(ang)) * n1;
    }
    

    public double getI() {
        return i;
    }

    public void setI(double i) {
        this.i = i;
    }

    public double getIz() {
        return iz;
    }

    public void setIz(double iz) {
        this.iz = iz;
    }

    public double getMalu_ang() {
        return this.malu_ang;
    }

    public void setMalu_ang(double ang) {
        this.malu_ang = ang;
    }

    public double getN1() {
        return n1;
    }

    public void setN1(double n1) {
        this.n1 = n1;
    }

    public double getN2() {
        return n2;
    }

    public void setN2(double n2) {
        this.n2 = n2;
    }

    public double getBrew_ang() {
        return brew_ang;
    }

    public void setBrew_ang(double brew_ang) {
        this.brew_ang = brew_ang;
    }
}