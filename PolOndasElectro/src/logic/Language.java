package logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import javax.swing.JOptionPane;

public class Language {

    private Map<String, Map<String, String>> languages;
    private String lang;
    private List<String> ficheiros;

    public Language() {
        lang = "EN";
        languages = new HashMap();
        ficheiros = new ArrayList();
        ficheiros.add("PT_PORTUGAL.txt");
        ficheiros.add("en_england.txt");
        insertLanguages();
    }

    public Language(String lang) {
        this.lang = lang.toUpperCase();
        languages = new HashMap();
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public Map<String, Map<String, String>> getLanguages() {
        return languages;
    }

    public void setLanguages(Map<String, Map<String, String>> languages) {
        this.languages = languages;
    }

    public String getValue(String key) {
        switch (this.lang) {
            case "PT":
                return languages.get("PT").get(key.toLowerCase());
            default:
                return languages.get("EN").get(key.toLowerCase());
        }
    }

    private void insertLanguages() {
        try {
            for (String s : ficheiros) {
                File f = new File(s);
                String linguagem = f.getName().split("_")[0].toUpperCase();
                if (languages.containsKey(linguagem)) {
                    languages.remove(linguagem);
                    Map<String, String> tempMap = new HashMap();
                    lerValores(tempMap, f);
                    languages.put(linguagem, tempMap);
                    return;
                }
                Map<String, String> tempMap = new HashMap();
                lerValores(tempMap, f);
                languages.put(linguagem, tempMap);
            }
        }catch(FileNotFoundException fnfex){
            JOptionPane.showMessageDialog(null, fnfex.getMessage(), "ERROR", JOptionPane.ERROR_MESSAGE);
        }
    }

    private void lerValores(Map<String, String> temp, File f) throws FileNotFoundException {
        Scanner in = new Scanner(f, "windows-1252");
        while (in.hasNextLine()) {
            String valores[] = in.nextLine().split(":");
            temp.put(valores[0].toLowerCase(), valores[1]);
        }
    }
}
