/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package data;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import logic.Calculus;

/**
 *
 * @author root
 */
public class BINARY {

    static public void save(Object o) {
        try {
            JFileChooser fc = new JFileChooser();
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int rtn = fc.showSaveDialog(null);
            if (rtn == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fc.getSelectedFile();
                FileOutputStream fileOut = new FileOutputStream(selectedFile.getAbsolutePath() + "data.ser");
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(o);
                out.close();
                fileOut.close();

                JOptionPane.showMessageDialog(null, "Saved!");
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error [000]", JOptionPane.ERROR_MESSAGE);
        }
    }
    static public void saveExit(Object o) {
        try {
            JFileChooser fc = new JFileChooser();
            fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int rtn = fc.showSaveDialog(null);
            if (rtn == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fc.getSelectedFile();
                FileOutputStream fileOut = new FileOutputStream(selectedFile.getAbsolutePath() + "data.ser");
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(o);
                out.close();
                fileOut.close();

                JOptionPane.showMessageDialog(null, "Saved!\nWill now exit");
                System.exit(0);
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error [000]", JOptionPane.ERROR_MESSAGE);
        }
    }

    static public Object open() {
        Object cc = null;
        try {
            JFileChooser fc = new JFileChooser();
            fc.setFileSelectionMode(JFileChooser.FILES_ONLY);
            int rtn = fc.showOpenDialog(null);
            if (rtn == JFileChooser.APPROVE_OPTION) {
                File selectedFile = fc.getSelectedFile();
                FileInputStream fileIn = new FileInputStream(selectedFile.getAbsolutePath());
                ObjectInputStream in = new ObjectInputStream(fileIn);
                cc = in.readObject();
                in.close();
                fileIn.close();
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "Error [000]", JOptionPane.ERROR_MESSAGE);
        }

        return cc;
    }

}
