package data;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Formatter;

public class HTML {
    public static void export(String file, String[] data) throws FileNotFoundException {
        Formatter output = new Formatter(new File(file));
        System.out.println(output.toString());
        output.format("%s%n", "<!doctype html><html><body>");
        for(int i = 0; i < (data.length / 6);i += 5){
            output.format("<h1>%s = %s x cos^2(%s)</h1><br />%n", data[i], data[i+1], data[i+2]);
            output.format("<h1>%s = arctan( %s / %s )</h1><br />%n", data[i+3], data[i+4], data[i+5]);
        }
        output.format("%s%n", "</body></html>");
        output.close();
    }
}
