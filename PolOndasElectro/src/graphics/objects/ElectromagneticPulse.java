package graphics.objects;

import graphics.models.GAnimate;
import graphics.models.GDraw;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;

public class ElectromagneticPulse extends GraphicElement{
    private Point start, end;

    public ElectromagneticPulse(GDraw g, Dimension w, Point start, Point end) {
        super(g, w);
        this.start = start;
        this.end = end;
    }

    @Override
    public void render() {
        this.graphics.drawLine(start, end, Color.BLACK);
    }

    @Override
    public void colision() {
        // not implemented yet
    }

    /**
     * @param start the start to set
     */
    public void setStart(Point start) {
        this.start = start;
    }

    /**
     * @param end the end to set
     */
    public void setEnd(Point end) {
        this.end = end;
    }

}
