package graphics.objects;

import graphics.models.GAnimate;
import graphics.models.GDraw;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

public class Eye extends GraphicElement{
    private Point position;
    private BufferedImage skin;
    private double scale;

    public Eye(GDraw g, Dimension w, Point position, double scale) {
        super(g, w);
        this.position = position;
        this.scale = scale;
        
        try{
            skin = ImageIO.read(new File("img/eye.png"));
        }catch(Exception ex){
            System.out.println("ERROR LOADING img/eye.png");
        }
    }

    @Override
    public void render() {
        graphics.drawImage(new Point(position.x, position.y),
                    (int)(skin.getWidth() * scale),
                    (int)(skin.getHeight() * scale),
                    skin);
    }

    @Override
    public void colision() {
        // not implemented yet
    }
}