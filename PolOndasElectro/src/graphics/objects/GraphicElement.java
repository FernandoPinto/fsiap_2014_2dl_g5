package graphics.objects;

import graphics.models.GAnimate;
import graphics.models.GDraw;
import java.awt.Dimension;

public abstract class GraphicElement {
    protected GDraw graphics;
    protected Dimension window_size;
    
    public GraphicElement(GDraw g, Dimension w){
        this.graphics = g;
        this.window_size = w;
    }

    public abstract void render();
    public abstract void colision();
}
