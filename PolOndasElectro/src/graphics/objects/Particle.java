package graphics.objects;

import graphics.models.GDraw;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Point;

public class Particle extends GraphicElement{
    public static final int DIR_UP = 1;
    public static final int DIR_DOWN = -1;
    public static final int DIR_RIGHT = 2;
    public static final int DIR_LEFT = -2;
    
    private Point position;
    private int r;
    private int direction_x;
    private int direction_y;

    public Particle(GDraw g, Dimension w, Point pt, int r) {
        super(g, w);
        this.position = pt;
        this.r = r;
        this.direction_x = 1;
        this.direction_y = 1;
    }

    @Override
    public void render() {
        // test colisions
        colision();
        //move
        position.x += direction_x;
        position.y += direction_y;
        //draw
        graphics.drawCircle(position, r, Color.red);
    }

    @Override
    public void colision() {
       if((position.x - r) <= 0 || (position.x + r) >= window_size.getWidth())
           direction_x *= -1;
       if((position.y - r) <= 0 || (position.y + r) >= window_size.getHeight())
           direction_y *= -1;
    }
}
