package graphics.objects;
import graphics.models.GAnimate;
import graphics.models.GDraw;
import java.awt.Dimension;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;

/**
 *
 * @author mrfhitz
 */
public class MyImage extends GraphicElement {
    private Point position;
    private String fileName = "";
    private BufferedImage skin;
    private double scale;
    
    public MyImage(GDraw g, Dimension w, Point position, double scale, String file) {
        super(g, w);
        this.position = position;
        this.scale = scale;
        
        try{
            this.skin = ImageIO.read(new File("img/"+file));
        }catch(Exception ex){
            System.out.println("ERROR LOADING img/"+file);
        }
        this.fileName = file;
    }

    @Override
    public void render() {
        graphics.drawImage(new Point(position.x, position.y),
                    (int)(skin.getWidth() * scale),
                    (int)(skin.getHeight() * scale),
                    skin);
    }

    @Override
    public void colision() {
        // not implemented yet
    }
    
    public boolean changeImage(String img){
        if(this.fileName.endsWith(img)) return false;
        this.fileName = img;
            
        try{
            this.skin = ImageIO.read(new File("img/"+img));
        }catch(Exception ex){
            System.out.println("ERROR LOADING img/"+img);
        }
        
        return true;
    }
}