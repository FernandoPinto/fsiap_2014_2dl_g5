package graphics;

import data.*;
import java.awt.*;
import java.awt.event.*;
import graphics.models.DemoPanel;
import graphics.models.LogicPanel;
import javax.swing.*;
import logic.*;
import java.io.File;

public class MainWindow extends JFrame {

    private LogicPanel logic;
    private DemoPanel demo;
    private Dimension size;
    private Language lang;

    public MainWindow(String window_name) {
        super(window_name);
        lang = new Language();
        init();
    }

    private void init() {
        size = new Dimension(800, 300);
        setSize(size);
        setLocationRelativeTo(null);
        //setUndecorated(true);

        setResizable(false);
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {

            @Override
            public void windowClosing(WindowEvent we) {
                exitnow();
            }

        });

        setLayout(new GridLayout(1, 2)); // 1 row 2 columns

        try {
            demo = new DemoPanel();
            logic = new LogicPanel(lang, demo);
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(this, ex.getMessage(), lang.getValue("errorio"), JOptionPane.ERROR_MESSAGE);
        }

        JMenuBar menubar = new JMenuBar();
        JMenu file = new JMenu(lang.getValue("file"));
        JMenu lang_menu = new JMenu(lang.getValue("language"));

        JMenuItem clear = new JMenuItem(lang.getValue("clearall"), KeyEvent.VK_C);
        clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                logic.reset();
            }
        });

        JMenuItem open = new JMenuItem(lang.getValue("open"), KeyEvent.VK_O);
        open.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Calculus c = (Calculus) BINARY.open();
                if(!(c == null)){
                logic.setCc(c);
                JOptionPane.showMessageDialog(null, lang.getValue("loaded"));
                }
            }
        });

        JMenuItem save = new JMenuItem(lang.getValue("save"), KeyEvent.VK_S);
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Calculus cc = logic.getCc();

                BINARY.save(cc);
            }

        });

        JMenuItem exportHTML = new JMenuItem(lang.getValue("exporthtml"), KeyEvent.VK_X);
        exportHTML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    String[] data = new String[6];
                    data[0] = logic.getText_i().getText();
                    data[1] = logic.getText_iz().getText();
                    data[2] = logic.getText_ang().getText();
                    data[3] = logic.getText_brew().getText();
                    data[4] = logic.getText_n2().getText();
                    data[5] = logic.getText_n1().getText();
                    
                    JFileChooser fc = new JFileChooser();
                    fc.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                    int rtn = fc.showOpenDialog(null);
                    if (rtn == JFileChooser.APPROVE_OPTION) {
                        File selectedFile = fc.getSelectedFile();
                        HTML.export(selectedFile.getAbsolutePath() + "/exportPol.html", data);
                        JOptionPane.showMessageDialog(null, lang.getValue("exportdone"));
                    } else if (rtn == JFileChooser.CANCEL_OPTION){
                    
                    } else {
                        throw new Exception(lang.getValue("invalidfile"));
                    }
                } catch (NumberFormatException ex) {
                    JOptionPane.showMessageDialog(null, lang.getValue("textfieldserror"), lang.getValue("error"), JOptionPane.ERROR_MESSAGE);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, ex.getMessage(), lang.getValue("error000"), JOptionPane.ERROR_MESSAGE);
                }
            }
        });

        JMenuItem exit = new JMenuItem(lang.getValue("exit"), KeyEvent.VK_S);
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                exitnow();
            }
        });
        
        JMenuItem en = new JMenuItem("EN", KeyEvent.VK_O);
        en.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changeLang("EN");
            }
        });
        
        JMenuItem pt = new JMenuItem("PT", KeyEvent.VK_O);
        pt.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                changeLang("PT");
            }
        });
        

        file.add(open);
        file.add(save);
        file.add(clear);
        file.add(exportHTML);
        file.add(exit);
        lang_menu.add(en);
        lang_menu.add(pt);
        menubar.add(file);
        menubar.add(lang_menu);
        setJMenuBar(menubar);

        this.add(logic);
        this.add(demo);

        setVisible(true);
    }
    
    private void changeLang(String lang) {
        this.lang.setLang(lang);
        this.setVisible(false);
        this.remove(this.logic);
        this.logic = new LogicPanel(this.lang, demo);
        this.add(logic, 0);
        this.revalidate();
        this.setVisible(true);
    }

    public void exitnow() {

        if (logic.isTextFieldsEmpty()) {
            System.exit(0);
        } else {
            int op = JOptionPane.showOptionDialog(null, lang.getValue("exitwarning"), lang.getValue("exit"), JOptionPane.YES_NO_CANCEL_OPTION, 3, null, null, null);
            if (op == JOptionPane.YES_OPTION) {
                Calculus cc = logic.getCc();
                BINARY.saveExit(cc);

            } else if (op == JOptionPane.NO_OPTION) {
                System.exit(0);
            }

        }
    }
}
