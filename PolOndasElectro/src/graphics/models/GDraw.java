package graphics.models;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.PageAttributes;
import java.awt.Point;
import java.awt.image.BufferedImage;

public class GDraw {
    private Graphics g;
    private Dimension d;
    private int frameInLastSecond = 0, framesInCurrentSecond = 0;
    private long nextSecond = System.currentTimeMillis() + 1000;
    
    public GDraw(Graphics g, Dimension d){
        this.g = g;
        this.d = d;
        
    }
    
    public void drawLine(Point a, Point b) {drawLine(a, b, null);}
    public void drawLine(Point a, Point b, Color c) {
        Color cl = g.getColor();
        if(c != null) g.setColor(c);
        g.drawLine(a.x, a.y, b.x, b.y);
        g.setColor(cl);
    }
    
    public void drawCircle(Point center, int r){drawCircle(center, r, null);}
    public void drawCircle(Point center, int r, Color c){
        int diam = (r * 2);
        Point up_corner = new Point(center.x - r, center.y - r);
        if(c != null){
            Color cl = g.getColor();
            g.setColor(c);
            g.fillOval(up_corner.x, up_corner.y, diam, diam);
            g.setColor(cl);
        }else g.drawOval(up_corner.x, up_corner.y, diam, diam);
        
    }
    
    public void drawOval(Point center, int w, int h){drawOval(center, w, h, null);}
    public void drawOval(Point center, int w, int h, Color c){
        Point up_corner = new Point(center.x - (w / 2), center.y - (h / 2));
        
        if(c != null){
            Color cl = g.getColor();
            g.setColor(c);
            g.fillOval(up_corner.x, up_corner.y, w, h);
            g.setColor(cl);
        }else g.drawOval(up_corner.x, up_corner.y, w, h);
        
    }
    
    
    public void drawRec(Point a, int w, int h){drawRec(a, w, h, null);}
    public void drawRec(Point a, int w, int h, Color c){
        if(c != null) {
            Color cl = g.getColor();
            g.setColor(c);
            g.fillRect(a.x, a.y, w, h);
            g.setColor(cl);
        }else g.drawRect(a.x, a.y, w, h);
    }
    
    public void drawImage(Point a, BufferedImage img){
        g.drawImage(img, a.x, a.y, null);
    }
    
    public void drawImage(Point a, int w, int h, BufferedImage img){
        g.drawImage(img, a.x, a.y, w, h, null);
    }
    
    public void drawString(String str, Point pt) {drawString(str, pt, null);}
    public void drawString(String str, Point pt, Font font) {
        Font ft = g.getFont();
        if(font != null) g.setFont(font);
        g.drawString(str, pt.x, pt.y);
        g.setFont(ft);
    }
    
    public void drawFPS(Point pos){
        long currentTime = System.currentTimeMillis();
        if (currentTime > nextSecond) {
            nextSecond += 1000;
            frameInLastSecond = framesInCurrentSecond;
            framesInCurrentSecond = 0;
        }
        framesInCurrentSecond++;
        
        Font ft = new Font(null, Font.BOLD, 15);
        drawString(frameInLastSecond + " fps", pos, ft);
    }
    
    public void clearAll(){
        g.clearRect(0, 0, d.width, d.height);
    }

    public void setSize(Dimension d) {
        this.d = d;
    }
    
    public void setGraphics(Graphics g) {
        this.g = g;
    }
}
