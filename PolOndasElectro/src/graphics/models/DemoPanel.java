package graphics.models;

import graphics.objects.*;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;

public class DemoPanel extends JPanel implements ActionListener{
    private Dimension size;
    private Timer t;
    private GDraw g;
    public int first_demo_status = 0, second_demo_status = 0;
    private ElectromagneticPulse in, out;
    
    private MyImage first_image, second_image;
    
    public DemoPanel() throws IOException {
        super(new BorderLayout());
        size = new Dimension(400, 250);
        setSize(size);
        
        init();
        
        t = new Timer(10, (ActionListener) this);
        t.start();
    }
    
    public void init(){
        g = new GDraw(null, size);
        
        // first demo
        first_image = new MyImage(g, size,
                new Point(20, 10), 1, "ref.png");
        
        //seconds demo
        second_image = new MyImage(g, size,
                new Point(0, 122), 1, second_demo_status+".png");
        
        // init tmp Electromagnetic pulses
        in = new ElectromagneticPulse(g, new Dimension(WIDTH, WIDTH), new Point(203,75), new Point(203,75));
        out = new ElectromagneticPulse(g, new Dimension(WIDTH, WIDTH), new Point(203,75), new Point(203,75));
    }
    
    @Override
    public void paintComponent(Graphics graph) {
        // default clear stuff
        g.setGraphics(graph);
        g.clearAll();
        
        // draw first demo
        first_image.render();
        
        // draw second demo
        second_image.changeImage(second_demo_status+".png");
        second_image.render();

        int val_x = (int) ((first_demo_status == 0) ? 0 : ((double)150.0 / ((90.0 / (double)first_demo_status))));
        int val_y = 150 - val_x;
        
        in.setStart(new Point(203 + val_x, 75 - val_y));
        in.render();
        g.drawString(first_demo_status+"º", new Point(170, 35));
        
        out.setStart(new Point(203 - val_x, 75 - val_y));
        out.render();
        g.drawString(first_demo_status+"º", new Point(227, 35));
        
        // debug FPS
        g.drawFPS(new Point(size.width - 50, 20));
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
    }
    
}
