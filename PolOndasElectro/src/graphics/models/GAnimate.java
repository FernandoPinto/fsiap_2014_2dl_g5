package graphics.models;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.HashMap;

public class GAnimate {
    private HashMap<String, Point> points;
    private HashMap<String, BufferedImage> images;
    
    public GAnimate(){
        points = new HashMap<>();
        images = new HashMap<>();
    }
    
    /******************************************
     *          ADD AND REMOVE OBJ'S          *
     ******************************************/
    
    public Point getAnimVar(String key){
        Point obj = points.get(key.toUpperCase());
        if(obj == null){
            Point n = new Point(0,0);
            addAnimVar(key, n);
            return n;
        }else return obj;
    }
    
    public Point addAnimVar(String key, Point val){
        return points.put(key.toUpperCase(), val);
    }
    
    public Point setAnimVar(String key, Point val){
        return addAnimVar(key, val);
    }
    
    public BufferedImage getImage(String key){
        return images.get(key.toUpperCase());
    }
    
    public BufferedImage addImage(String key, BufferedImage obj){
        return images.put(key.toUpperCase(), obj);
    }
    
    
    /******************************************
     *     ADD MOVEMENT TO SOME KEY VALUE     *
     ******************************************/
    
    public Point addToX(String key, int val){
        Point p = getAnimVar(key);
        p.x += val;
        return p;
    }
    
    public Point addToY(String key, int val){
        Point p = getAnimVar(key);
        p.y += val;
        return p;
    }
    
    public Point addToXY(String key, int xval, int yval){
        Point p = getAnimVar(key);
        p.x += xval;
        p.y += yval;
        return p;
    }
    
    public Point addToXY(String key, int val){
        return addToXY(key, val, val);
    }
    
    public Point subToX(String key, int val){
        Point p = getAnimVar(key);
        p.x -= val;
        return p;
    }
    
    public Point subToY(String key, int val){
        Point p = getAnimVar(key);
        p.y -= val;
        return p;
    }
    
    public Point subToXY(String key, int xval, int yval){
        Point p = getAnimVar(key);
        p.x -= xval;
        p.y -= yval;
        return p;
    }
    
    public Point subToXY(String key, int val){
        return subToXY(key, val, val);
    }
}
