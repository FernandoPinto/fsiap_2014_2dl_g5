package graphics.models;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.text.DecimalFormat;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import logic.Calculus;
import logic.Language;
import graphics.models.DemoPanel;

public class LogicPanel extends JPanel{
    private JTextField text_i, text_n1;
    private JTextField text_iz, text_n2;
    private JTextField text_ang, text_brew;
    private JLabel label_i, label_iz, label_ang;
    private JLabel label_n1, label_n2, label_brew;
    private JLabel exp_int, exp_brew;
    private JLabel title1, title2;
    private JButton bt_clear;
    
    private DecimalFormat df = new DecimalFormat("#.##");
    private Calculus cc = new Calculus();
    
    private Language lang;
    private DemoPanel demo;
    
    public LogicPanel(Language lang, DemoPanel demo) {
        super(new BorderLayout()); // rows col
        this.lang = lang;
        this.demo = demo;
        init();
    }
    
    private void init(){
        JPanel maingrid = new JPanel(new GridLayout(3, 1));
        JPanel calcgrid1 = new JPanel(new GridLayout(1, 3));
        JPanel calcgrid2 = new JPanel(new GridLayout(1, 3));
        JPanel brew = new JPanel(new BorderLayout());
        JPanel malu = new JPanel(new BorderLayout());
        
        bt_clear = new JButton(lang.getValue("clear"));
        
        bt_clear.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                reset();
            }
        });
        
        label_i = new JLabel("I");
        label_iz = new JLabel("I0");
        label_ang = new JLabel("Å");
        exp_int = new JLabel("I = I0 * cos^2(Å)", SwingConstants.CENTER);
        getExp().setFont(new Font("Arial", Font.BOLD, 20));
        title1 = new JLabel(lang.getValue("malustitle"), SwingConstants.CENTER);
        title1.setFont(new Font("Arial", Font.BOLD, 25));
        
        label_n1 = new JLabel("N1");
        label_n2 = new JLabel("N2");
        label_brew = new JLabel("ß"); 
        exp_brew = new JLabel("ß = arctg( N2 / N1 )", SwingConstants.CENTER);
        exp_brew.setFont(new Font("Arial", Font.BOLD, 20));
        title2 = new JLabel(lang.getValue("brewstertitle"), SwingConstants.CENTER);
        title2.setFont(new Font("Arial", Font.BOLD, 25));
        
        setText_i(new JTextField(4));
        setText_iz(new JTextField(4));
        setText_ang(new JTextField(7));
        
        setText_n1(new JTextField(4));
        setText_n2(new JTextField(4));
        setText_brew(new JTextField(7));
        
        TextEvents eventListener = new TextEvents();
        
        getText_i().addFocusListener(eventListener);
        getText_iz().addFocusListener(eventListener);
        getText_ang().addFocusListener(eventListener);
        
        getText_n1().addFocusListener(eventListener);
        getText_n2().addFocusListener(eventListener);
        getText_brew().addFocusListener(eventListener);
        
        
        JPanel i = new JPanel(new FlowLayout(FlowLayout.LEFT));
        i.add(label_i);
        i.add(getText_i());
        
        JPanel iz = new JPanel(new FlowLayout(FlowLayout.LEFT));
        iz.add(label_iz);
        iz.add(getText_iz());
        
        JPanel ang = new JPanel(new FlowLayout(FlowLayout.LEFT));
        ang.add(label_ang);
        ang.add(getText_ang());
        
        
        calcgrid1.add(i);
        calcgrid1.add(iz);
        calcgrid1.add(ang);
        
        malu.add(title1, BorderLayout.NORTH);
        malu.add(calcgrid1, BorderLayout.CENTER);
        malu.add(exp_int, BorderLayout.SOUTH);
        
        JPanel n1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        n1.add(label_n1);
        n1.add(getText_n1());
        
        JPanel n2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        n2.add(label_n2);
        n2.add(getText_n2());
        
        JPanel brew_ang = new JPanel(new FlowLayout(FlowLayout.LEFT));
        brew_ang.add(label_brew);
        brew_ang.add(getText_brew());
        
        
        calcgrid2.add(n1);
        calcgrid2.add(n2);
        calcgrid2.add(brew_ang);
        
        brew.add(title2, BorderLayout.NORTH);
        brew.add(calcgrid2, BorderLayout.CENTER);
        brew.add(exp_brew, BorderLayout.SOUTH);

        maingrid.add(brew);
        maingrid.add(bt_clear);
        maingrid.add(malu);
        
        add(maingrid, BorderLayout.CENTER);
    }
    
    public void reset() {
        getExp().setText("I = I0 * cos^2(Å)");
        getText_i().setText("");
        getText_iz().setText("");
        getText_ang().setText("");
        
        getText_i().setEditable(true);
        getText_iz().setEditable(true);
        getText_ang().setEditable(true);
        
        getText_brew().setText("ß = arctg( N2 / N1 )");
        getText_n1().setText("");
        getText_n2().setText("");
        getText_brew().setText("");
        
        getText_n1().setEditable(true);
        getText_n2().setEditable(true);
        getText_brew().setEditable(true);
    }
    
    private void updateExp(){
        String str = "";
        str += (!text_i.getText().trim().isEmpty()) ? getText_i().getText() : "I";
        str += " = ";
        str += (!text_iz.getText().trim().isEmpty()) ? getText_iz().getText() : "I0";
        str += " * cos^2(";
        str += (!text_ang.getText().trim().isEmpty()) ? getText_ang().getText() : "Å";
        str += ")";
        
        getExp().setText(str);
        
        
        str = (!text_brew.getText().trim().isEmpty()) ? getText_brew().getText() : "ß";
        str += " = arctg( ";
        str += (!text_n2.getText().trim().isEmpty()) ? getText_n2().getText() : "N2";
        str += " / ";
        str += (!text_n1.getText().trim().isEmpty()) ? getText_n1().getText() : "N1";
        str += " )";
        
        exp_brew.setText(str);
    }
    
    public boolean isTextFieldsEmpty(){
        return (text_i.getText().trim().isEmpty() &&
                text_iz.getText().trim().isEmpty() &&
                text_ang.getText().trim().isEmpty() &&
                getText_n1().getText().trim().isEmpty() &&
                getText_n2().getText().trim().isEmpty() &&
                getText_brew().getText().trim().isEmpty());
    }

    public JTextField getText_i() {
        return text_i;
    }

    public void setText_i(JTextField text_i) {
        this.text_i = text_i;
    }

    public JTextField getText_iz() {
        return text_iz;
    }

    public void setText_iz(JTextField text_iz) {
        this.text_iz = text_iz;
    }

    public JTextField getText_ang() {
        return text_ang;
    }

    public void setText_ang(JTextField text_ang) {
        this.text_ang = text_ang;
    }

    public JLabel getExp() {
        return exp_int;
    }

    public Calculus getCc() {
        return cc;
    }

    public void setCc(Calculus cc) {
        this.cc = cc;
        if(cc.getI() == 0){
            getText_i().setText("");
        } else {
        getText_i().setText(df.format(cc.getI())+"");
        }
        if(cc.getIz() == 0){
        getText_iz().setText("");
        } else {
        getText_iz().setText(df.format(cc.getIz())+"");
        }
        if(cc.getMalu_ang() == 0){
            getText_ang().setText("");
        } else {
            getText_ang().setText(df.format(cc.getMalu_ang())+"");
        }
        if(cc.getN1() == 0){
            getText_n1().setText("");
        } else {
            getText_n1().setText(df.format(cc.getN1())+"");
        }
        if(cc.getN2() == 0){
            getText_n2().setText("");
        } else {
            getText_n2().setText(df.format(cc.getN2())+"");
        }
        if(cc.getBrew_ang() == 0){
            getText_brew().setText("");
        } else {
            getText_brew().setText(df.format(cc.getBrew_ang())+"");
        }
    }

    public JTextField getText_n1() {
        return text_n1;
    }

    public void setText_n1(JTextField text_n1) {
        this.text_n1 = text_n1;
    }

    public JTextField getText_n2() {
        return text_n2;
    }

    public void setText_n2(JTextField text_n2) {
        this.text_n2 = text_n2;
    }

    public JTextField getText_brew() {
        return text_brew;
    }

    public void setText_brew(JTextField text_brew) {
        this.text_brew = text_brew;
    }
    
    private class TextEvents implements FocusListener {

        @Override
        public void focusGained(FocusEvent e) {}

        @Override
        public void focusLost(FocusEvent e) {
            this.checkMalu();
            this.checkBrew();
        }
        
        private void checkMalu(){
            try{
                short count = 0;
                String s1, s2, s3;

                if(getText_i().getText().trim().isEmpty()) {
                    count++;
                    s1 = null;
                }else s1 = getText_i().getText().trim();
                if(getText_iz().getText().trim().isEmpty()) {
                    count++;
                    s2 = null;
                }else s2 = getText_iz().getText().trim();
                if(getText_ang().getText().trim().isEmpty()) {
                    count++;
                    s3 = null;
                }else s3 = getText_ang().getText().trim();

                double d1, d2, d3;

                
                
                if(count == 1) {
                    if(s1 == null){
                        d2 = Double.parseDouble(s2);
                        d3 = Double.parseDouble(s3);
                        d1 = Calculus.Intensity(null, d2, d3);
                        getText_i().setText(df.format(d1));
                    }else if(s2 == null) {
                        d1 = Double.parseDouble(s1);
                        d3 = Double.parseDouble(s3);
                        d2 = Calculus.Intensity(d1, null, d3);
                        getText_iz().setText(df.format(d2));
                    }else {
                        d1 = Double.parseDouble(s1);
                        d2 = Double.parseDouble(s2);
                        d3 = Calculus.Intensity(d1, d2, null);
                        getText_ang().setText(df.format(d3));
                    }
                    
                    getCc().setI(d1);getCc().setIz(d2);getCc().setMalu_ang(d3);
                    
                    int res = (int) Math.round(100 - Math.abs((d1 / d2) * 100));
                    if(res < 50) {
                        demo.second_demo_status = 50;
                    }else if(res >= 50 && res < 60){
                        demo.second_demo_status = 50;
                    }else if(res >= 60 && res < 70){
                        demo.second_demo_status = 60;
                    }else if(res >= 70 && res < 80){
                        demo.second_demo_status = 70;
                    }else if(res >= 80 && res < 90){
                        demo.second_demo_status = 80;
                    }else if(res >= 90 && res < 100){
                        demo.second_demo_status = 90;
                    }else {
                        demo.second_demo_status = 100;
                    }
                    
                }
                updateExp();
                
                if(!text_i.getText().isEmpty() && !text_iz.getText().isEmpty() && !text_ang.getText().isEmpty()){
                    getText_ang().setEditable(false);
                    getText_i().setEditable(false);
                    getText_iz().setEditable(false);
                }
                
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "ERROR: " + ex.getMessage());
                reset();
            }
        }
        
        private void checkBrew(){
            try{
                short count = 0;
                String s1, s2, s3;

                if(getText_n1().getText().trim().isEmpty()) {
                    count++;
                    s1 = null;
                }else s1 = getText_n1().getText().trim();
                if(getText_n2().getText().trim().isEmpty()) {
                    count++;
                    s2 = null;
                }else s2 = getText_n2().getText().trim();
                if(getText_brew().getText().trim().isEmpty()) {
                    count++;
                    s3 = null;
                }else s3 = getText_brew().getText().trim();

                double n1, n2, ang;

                
                
                if(count == 1) {
                    if(s1 == null){
                        n2 = Double.parseDouble(s2);
                        ang = Double.parseDouble(s3);
                        n1 = Calculus.Brewster(ang, null, n2);
                        getText_n1().setText(df.format(n1));
                    }else if(s2 == null) {
                        n1 = Double.parseDouble(s1);
                        ang = Double.parseDouble(s3);
                        n2 = Calculus.Brewster(ang, n1, null);
                        getText_n2().setText(df.format(n2));
                    }else {
                        n1 = Double.parseDouble(s1);
                        n2 = Double.parseDouble(s2);
                        ang = Calculus.Brewster(null, n1, n2);
                        getText_brew().setText(df.format(ang));
                    }
                    
                    getCc().setN1(n1);getCc().setN2(n2);getCc().setBrew_ang(ang);
                    
                    demo.first_demo_status = Math.round((float)ang);
                }
                updateExp();
                
                if(!text_n1.getText().isEmpty() && !text_n2.getText().isEmpty() && !text_brew.getText().isEmpty()){
                    getText_n1().setEditable(false);
                    getText_n2().setEditable(false);
                    getText_brew().setEditable(false);
                }
                
            }catch(Exception ex){
                JOptionPane.showMessageDialog(null, "ERROR: " + ex.getMessage());
                reset();
            }
        }
    }
    
}
