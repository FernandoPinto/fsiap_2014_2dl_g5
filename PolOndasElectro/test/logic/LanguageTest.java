/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logic;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author RenatoAyres
 */
public class LanguageTest {

    public LanguageTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getLang method, of class Language.
     */
    @Test
    public void testGetLang() throws FileNotFoundException {
        System.out.println("getLang");
        Language instance = new Language();
        String expResult = "EN";
        String result = instance.getLang();
        assertEquals(expResult, result);
    }

    /**
     * Test of getLanguages method, of class Language.
     */
    @Test
    public void testGetLanguages() throws FileNotFoundException {
        System.out.println("getLanguages");
        Language instance = new Language();
        
        Map<String, Map<String, String>> expResult = new HashMap();
        
        Map<String, Map<String, String>> result = instance.getLanguages();
        assertEquals(expResult, result);
    }

    /**
     * Test of getValue method, of class Language.
     */
    @Test
    public void testGetValue() throws FileNotFoundException {
        System.out.println("getValue");
        String key = "file";
        Language instance = new Language();
        String expResult = "File";
        String result = instance.getValue(key);
        assertEquals(expResult, result);
    }
}
